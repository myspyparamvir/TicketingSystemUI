///<reference path="../../../../../../node_modules/rxjs/internal/Observable.d.ts"/>
import {Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
// import {HttpClient, HttpClientModule} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ServerService} from './server.service';
import {SingleresultComponent} from './singleresult/singleresult.component';


@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {

  searchResults: SingleresultComponent[] = new Array();
  public departmentList: IDepartment[] = new Array();

  ngOnInit() {
  }

  searchDepartmentsWithName(departmentName: string) {
    this.serverService.searchDepartment(departmentName).subscribe( (response) => {
        console.log(response);
      },
      (error) => console.log(error)
    );
    this.prepareSearchResults();
  }
  /*
  searchDepartmentsWithName(departmentName: string) {
    this.serverService.searchDepartment(departmentName).subscribe( (response) => {
      const data = response.json();
      console.log(data);
      },
      (error) => console.log(error)
      );
  }
  */
  prepareSearchResults() {
    this.searchResults  = new Array();
    console.log('in prepareSearchResults. length of department list' + this.serverService.departmentList.length)
    for (const dept of this.departmentList) {
      const department: IDepartment = <IDepartment> dept;
      var result: SingleresultComponent = new SingleresultComponent();
      result.departmentId =  department.departmentId;
      result.departmentName = department.departmentName;
      console.log('adding' + result);
      this.searchResults.push(result);
    }
    console.log('search results .........');
    console.log(this.searchResults.length)
    for (const dept of this.searchResults) {
      console.log('department found' + dept.departmentName);
    }
    console.log('search results .........');

  }
  constructor(public serverService: ServerService) {
    this.serverService.departmentListEventEmitter.subscribe(
      ( dList: IDepartment[] ) => { this.departmentList = dList; }
    );
  }
  onClick(clickedResult: SingleresultComponent ) {
    console.log('Clicked On ' + clickedResult.departmentId);
    this.serverService.clickedDepartmentNumber.emit(clickedResult.departmentId);
  }
}
export interface IDepartment {
  departmentId: number;
  departmentName: string;
  departmentCode: string;
}
