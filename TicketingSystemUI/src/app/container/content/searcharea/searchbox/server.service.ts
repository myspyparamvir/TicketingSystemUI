import {EventEmitter, Injectable} from '@angular/core' ;
import {Headers, Http, Response} from '@angular/http';
import 'rxjs/Rx';
import {SingleresultComponent} from './singleresult/singleresult.component';
import {Observable} from 'rxjs/Observable';
import {TicketSummary} from '../../departmenttickets/departmenttickets.component';


@Injectable()
export class ServerService {
  constructor(private http: Http) {
  }
   public departmentList: IDepartment[];
  public departmentListEventEmitter = new EventEmitter<IDepartment[]>();
  public clickedDepartmentNumber = new EventEmitter<number>();
  public ticketSummary = new EventEmitter<TicketSummary[]>();
   searchDepartment(name: string) {
     this.departmentList = new Array();
    const url = `http://13.58.214.236:8082/request/searchDepartmentsByName/${name}`;
    return this.http.get(url).map(
      (response: Response) => {
        const data = response.json();
        for (const temp of data) {
          console.log(temp);
            // const jsonObj: any = JSON.parse(temp);
            const department: IDepartment = <IDepartment> temp;
            this.departmentList.push(department);
            console.log('pushing department' + department.departmentName);
        }
        this.departmentListEventEmitter.emit(this.departmentList);
        console.log(this.departmentList.length);
      }
    );
  }

  retrieveTicketsForDepartment(departmentNumber: number) {
     console.log('in ServerService.retrieveTicketsForDepartment ');
    const retrievePendingDepartmentTicketsUrl = `http://13.58.214.236:8082/request/retrievePendingDepartmentTickets/${departmentNumber}`;
    return this.http.get(retrievePendingDepartmentTicketsUrl).map(
      ( response: Response ) => {
        console.log(response);
        const tickets = response.json();
        var tSummary =  new Array();
        for (const ticket of tickets) {
          const currentTicket: TicketSummary = <TicketSummary> ticket;
          tSummary.push(currentTicket);
          console.log(tickets);
        }
        this.ticketSummary.emit( tSummary);
      }
    );
  }
}

export interface IDepartment {
  departmentId: number;
  departmentName: string;
  departmentCode: string;
}

