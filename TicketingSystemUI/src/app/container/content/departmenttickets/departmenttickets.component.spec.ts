import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentticketsComponent } from './departmenttickets.component';

describe('DepartmentticketsComponent', () => {
  let component: DepartmentticketsComponent;
  let fixture: ComponentFixture<DepartmentticketsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentticketsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentticketsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
