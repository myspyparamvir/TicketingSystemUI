import {Component, EventEmitter, OnInit} from '@angular/core';
import {ServerService} from '../searcharea/searchbox/server.service';

@Component({
  selector: 'app-departmenttickets',
  templateUrl: './departmenttickets.component.html',
  styleUrls: ['./departmenttickets.component.css']
})
export class DepartmentticketsComponent implements OnInit {

  departmentId: number;
  public ticketSummary: TicketSummary[];


  constructor(public serverService: ServerService) {
    this.serverService.clickedDepartmentNumber.subscribe(
      ( deptNumber: number ) => {
        this.departmentId = deptNumber;
        this.serverService.retrieveTicketsForDepartment(deptNumber).subscribe( (response) => console.log(response),
            (error) => console.log(error)
        );
        }
    );
    this.serverService.ticketSummary.subscribe(
      (tSummary: TicketSummary[]) => {this.ticketSummary = tSummary; }
    );
  }

  ngOnInit() {
  }
}

export interface TicketSummary {
  ticketNumber: number;
  status: string;
  assignedTo: Employee;
  description: string;
  createdBy: number;
  creationTime: string;
  assignTime: string;
  closingTime: any;
  assignedToDepartment: number;
  closedBy: any;
}

export interface Employee{
  employeeId: number;
  name: string;
  designation: string;
  managerEmployeeId: number;
  department: number;
}

