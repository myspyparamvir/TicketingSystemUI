import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ContainerComponent } from './container/container.component';
import { HeaderComponent } from './container/header/header.component';
import { ContentComponent } from './container/content/content.component';
import { TabsComponent } from './container/content/tabs/tabs.component';
import { TabComponent } from './container/content/tabs/tab/tab.component';
import { SearchareaComponent } from './container/content/searcharea/searcharea.component';
import { SearchboxComponent } from './container/content/searcharea/searchbox/searchbox.component';
import {HttpClientModule} from '@angular/common/http';
import {HttpModule} from '@angular/http';

import {ServerService} from './container/content/searcharea/searchbox/server.service';
import { SingleresultComponent } from './container/content/searcharea/searchbox/singleresult/singleresult.component';
import { DepartmentticketsComponent } from './container/content/departmenttickets/departmenttickets.component';

@NgModule({
  declarations: [
    AppComponent,
    ContainerComponent,
    HeaderComponent,
    ContentComponent,
    TabsComponent,
    TabComponent,
    SearchareaComponent,
    SearchboxComponent,
    SingleresultComponent,
    DepartmentticketsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
  ],
  providers: [ServerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
